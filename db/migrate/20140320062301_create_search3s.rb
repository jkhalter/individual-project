class CreateSearch3s < ActiveRecord::Migration
  def change
    create_table :search3s do |t|
      t.integer :item_id

      t.timestamps
    end
  end
end
