class CreateSearch4s < ActiveRecord::Migration
  def change
    create_table :search4s do |t|
      t.integer :item_id

      t.timestamps
    end
  end
end
