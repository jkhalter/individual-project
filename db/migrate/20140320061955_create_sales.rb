class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :item_id
      t.date :sale_date
      t.integer :amount_sold

      t.timestamps
    end
  end
end
