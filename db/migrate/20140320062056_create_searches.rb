class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.integer :item_id
      t.date :s_date
      t.date :e_date

      t.timestamps
    end
  end
end
