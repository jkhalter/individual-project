class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :item_name
      t.float :price
      t.integer :amount
      t.integer :min_amount

      t.timestamps
    end
  end
end
