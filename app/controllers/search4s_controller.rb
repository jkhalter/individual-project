class Search4sController < ApplicationController
  before_action :set_search4, only: [:show, :edit, :update, :destroy]

  # GET /search4s
  # GET /search4s.json
  def index
    @search4s = Search4.all
  end

  # GET /search4s/1
  # GET /search4s/1.json
  def show
    @search4 = Search4.find(params[:id])
  end

  # GET /search4s/new
  def new
    @search4 = Search4.new
  end

  # GET /search4s/1/edit
  def edit
  end

  # POST /search4s
  # POST /search4s.json
  def create
    @search4 = Search4.new(search4_params)

    respond_to do |format|
      if @search4.save
        format.html { redirect_to @search4, notice: 'Search4 was successfully created.' }
        format.json { render action: 'show', status: :created, location: @search4 }
      else
        format.html { render action: 'new' }
        format.json { render json: @search4.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /search4s/1
  # PATCH/PUT /search4s/1.json
  def update
    respond_to do |format|
      if @search4.update(search4_params)
        format.html { redirect_to @search4, notice: 'Search4 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @search4.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /search4s/1
  # DELETE /search4s/1.json
  def destroy
    @search4.destroy
    respond_to do |format|
      format.html { redirect_to search4s_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_search4
    @search4 = Search4.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def search4_params
    params.require(:search4).permit(:item_id)
  end
end
