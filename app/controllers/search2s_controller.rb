class Search2sController < ApplicationController
  before_action :set_search2, only: [:show, :edit, :update, :destroy]

  # GET /search2s
  # GET /search2s.json
  def index
    @search2s = Search2.all
  end

  # GET /search2s/1
  # GET /search2s/1.json
  def show
    @search2 = Search2.find(params[:id])
  end

  # GET /search2s/new
  def new
    @search2 = Search2.new
  end

  # GET /search2s/1/edit
  def edit
  end

  # POST /search2s
  # POST /search2s.json
  def create
    @search2 = Search2.new(search2_params)

    respond_to do |format|
      if @search2.save
        format.html { redirect_to @search2, notice: 'Search2 was successfully created.' }
        format.json { render action: 'show', status: :created, location: @search2 }
      else
        format.html { render action: 'new' }
        format.json { render json: @search2.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /search2s/1
  # PATCH/PUT /search2s/1.json
  def update
    respond_to do |format|
      if @search2.update(search2_params)
        format.html { redirect_to @search2, notice: 'Search2 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @search2.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /search2s/1
  # DELETE /search2s/1.json
  def destroy
    @search2.destroy
    respond_to do |format|
      format.html { redirect_to search2s_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_search2
    @search2 = Search2.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def search2_params
    params.require(:search2).permit(:item_id, :s_date, :e_date)
  end
end
