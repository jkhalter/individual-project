class Search3sController < ApplicationController
  before_action :set_search3, only: [:show, :edit, :update, :destroy]

  # GET /search3s
  # GET /search3s.json
  def index
    @search3s = Search3.all
  end

  # GET /search3s/1
  # GET /search3s/1.json
  def show
    @search3 = Search3.find(params[:id])
  end

  # GET /search3s/new
  def new
    @search3 = Search3.new
  end

  # GET /search3s/1/edit
  def edit
  end

  # POST /search3s
  # POST /search3s.json
  def create
    @search3 = Search3.new(search3_params)

    respond_to do |format|
      if @search3.save
        format.html { redirect_to @search3, notice: 'Search3 was successfully created.' }
        format.json { render action: 'show', status: :created, location: @search3 }
      else
        format.html { render action: 'new' }
        format.json { render json: @search3.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /search3s/1
  # PATCH/PUT /search3s/1.json
  def update
    respond_to do |format|
      if @search3.update(search3_params)
        format.html { redirect_to @search3, notice: 'Search3 was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @search3.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /search3s/1
  # DELETE /search3s/1.json
  def destroy
    @search3.destroy
    respond_to do |format|
      format.html { redirect_to search3s_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_search3
    @search3 = Search3.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def search3_params
    params.require(:search3).permit(:item_id)
  end
end
