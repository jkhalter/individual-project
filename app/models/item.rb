class Item < ActiveRecord::Base
  has_many :sales
  has_many :searches
  has_many :search2s
  has_many :search3s
  has_many :search4s
  validates :amount, :numericality => { :greater_than_or_equal_to => 0 }
  validates_presence_of :item_name, :price, :amount, :min_amount

end