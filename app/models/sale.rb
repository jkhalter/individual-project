class Sale < ActiveRecord::Base
  belongs_to :item
  accepts_nested_attributes_for :item
  validates :amount_sold, :numericality => { :greater_than_or_equal_to => 0 }
  validates_presence_of :item_id, :sale_date, :amount_sold





end
