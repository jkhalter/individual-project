class Search3 < ActiveRecord::Base
  belongs_to :item
  accepts_nested_attributes_for :item

  def items3
    @items3 ||= find_items
    #@items2 ||= find_items2
  end

  private
  def find_items
    items = Item.where(:id => item_id) if item_id.present?
    #items = Sale.where('sale_date >= ?' => s_date) if s_date.present?
    #items = Sale.where('sale_date <= ?' =>  e_date) if e_date.present?
    items
  end

  #def find_items2
  #  items = Sale.where('sale_date >= ?' => s_date) if s_date.present?
  #  items = Sale.where('sale_date <= ?' =>  e_date) if e_date.present?
  #  items
  #end

end

