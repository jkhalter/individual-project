json.array!(@search3s) do |search3|
  json.extract! search3, :id, :item_id
  json.url search3_url(search3, format: :json)
end
