json.array!(@searches) do |search|
  json.extract! search, :id, :item_id, :s_date, :e_date
  json.url search_url(search, format: :json)
end
