json.array!(@search2s) do |search2|
  json.extract! search2, :id, :item_id, :s_date, :e_date
  json.url search2_url(search2, format: :json)
end
