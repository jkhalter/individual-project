json.array!(@sales) do |sale|
  json.extract! sale, :id, :item_id, :sale_date, :amount_sold
  json.url sale_url(sale, format: :json)
end
