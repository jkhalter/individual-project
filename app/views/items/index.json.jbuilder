json.array!(@items) do |item|
  json.extract! item, :id, :item_name, :price, :amount, :min_amount
  json.url item_url(item, format: :json)
end
