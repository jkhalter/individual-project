json.array!(@search4s) do |search4|
  json.extract! search4, :id, :item_id
  json.url search4_url(search4, format: :json)
end
