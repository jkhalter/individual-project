require 'test_helper'

class Search4sControllerTest < ActionController::TestCase
  setup do
    @search4 = search4s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:search4s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create search4" do
    assert_difference('Search4.count') do
      post :create, search4: { item_id: @search4.item_id }
    end

    assert_redirected_to search4_path(assigns(:search4))
  end

  test "should show search4" do
    get :show, id: @search4
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @search4
    assert_response :success
  end

  test "should update search4" do
    patch :update, id: @search4, search4: { item_id: @search4.item_id }
    assert_redirected_to search4_path(assigns(:search4))
  end

  test "should destroy search4" do
    assert_difference('Search4.count', -1) do
      delete :destroy, id: @search4
    end

    assert_redirected_to search4s_path
  end
end
