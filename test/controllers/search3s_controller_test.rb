require 'test_helper'

class Search3sControllerTest < ActionController::TestCase
  setup do
    @search3 = search3s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:search3s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create search3" do
    assert_difference('Search3.count') do
      post :create, search3: { item_id: @search3.item_id }
    end

    assert_redirected_to search3_path(assigns(:search3))
  end

  test "should show search3" do
    get :show, id: @search3
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @search3
    assert_response :success
  end

  test "should update search3" do
    patch :update, id: @search3, search3: { item_id: @search3.item_id }
    assert_redirected_to search3_path(assigns(:search3))
  end

  test "should destroy search3" do
    assert_difference('Search3.count', -1) do
      delete :destroy, id: @search3
    end

    assert_redirected_to search3s_path
  end
end
