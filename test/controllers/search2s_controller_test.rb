require 'test_helper'

class Search2sControllerTest < ActionController::TestCase
  setup do
    @search2 = search2s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:search2s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create search2" do
    assert_difference('Search2.count') do
      post :create, search2: { e_date: @search2.e_date, item_id: @search2.item_id, s_date: @search2.s_date }
    end

    assert_redirected_to search2_path(assigns(:search2))
  end

  test "should show search2" do
    get :show, id: @search2
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @search2
    assert_response :success
  end

  test "should update search2" do
    patch :update, id: @search2, search2: { e_date: @search2.e_date, item_id: @search2.item_id, s_date: @search2.s_date }
    assert_redirected_to search2_path(assigns(:search2))
  end

  test "should destroy search2" do
    assert_difference('Search2.count', -1) do
      delete :destroy, id: @search2
    end

    assert_redirected_to search2s_path
  end
end
